LIBRARY ieee;
USE ieee.std_logic_1164.all;

ENTITY main IS
	PORT(clk :in std_logic;
		  serial :in std_logic;
		  reset :in std_logic;
		  cifra0 :out std_logic_vector(6 DOWNTO 0);
		  cifra1 :out std_logic_vector(6 DOWNTO 0));
END;

ARCHITECTURE arch_main OF main IS
	
	COMPONENT serialeParallelo
		PORT(serial :in  std_logic; --serial deve cambiare valore sul fronte di salita del clock
			  clk    :in  std_logic; 
			  reset  :in  std_logic;
			  ready  :out std_logic;
			  data   :out std_logic_vector(7 DOWNTO 0));
	END COMPONENT;
	
	COMPONENT decoder
		PORT(clk   :in std_logic;
			  data  :in std_logic_vector(3 DOWNTO 0);
			  ready :in std_logic;
			  cifra :out std_logic_vector(6 DOWNTO 0));
	END COMPONENT;
	
	SIGNAL tempData :std_logic_vector(7 DOWNTO 0);
	SIGNAL tempDataLow, tempDataHigh :std_logic_vector(3 DOWNTO 0);
	SIGNAL tempReady :std_logic;
	SIGNAL tempCifra0, tempCifra1 :std_logic_vector(6 DOWNTO 0);
	
	BEGIN
	
	x1: serialeParallelo PORT MAP (serial, clk, reset, tempReady, tempData);
	
	tempDataLow  <= tempData (3 DOWNTO 0);
	tempDataHigh <= tempData (7 DOWNTO 4);
	
	x2: decoder PORT MAP (clk, tempDataLow, tempReady, tempCifra0);
	x3: decoder PORT MAP (clk, tempDataHigh, tempReady, tempCifra1);
	
	PROCESS (tempReady)
	
		BEGIN
			IF tempReady = '1' THEN
				cifra0 <= tempCifra0;
				cifra1 <= tempCifra1;
			ELSE ---nel caso in cui non ho il dato disponibile corretto, mantengo i led spenti
				cifra0 <= "1111111";
				cifra1 <= "1111111";
			END IF;
		
	
		END PROCESS;
	
	
	
	END;