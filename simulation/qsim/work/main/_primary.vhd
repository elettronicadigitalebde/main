library verilog;
use verilog.vl_types.all;
entity main is
    port(
        clk             : in     vl_logic;
        serial          : in     vl_logic;
        reset           : in     vl_logic;
        cifra0          : out    vl_logic_vector(6 downto 0);
        cifra1          : out    vl_logic_vector(6 downto 0)
    );
end main;
