transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vcom -93 -work work {D:/UNIVERSITA/magistrale/2a1s/ElettronicaDigitale/progetto/decoder/decoder.vhd}
vcom -93 -work work {D:/UNIVERSITA/magistrale/2a1s/ElettronicaDigitale/progetto/serialeParallelo/serialeParallelo.vhd}
vcom -93 -work work {D:/UNIVERSITA/magistrale/2a1s/ElettronicaDigitale/progetto/main/main.vhd}

vcom -93 -work work {D:/UNIVERSITA/magistrale/2a1s/ElettronicaDigitale/progetto/main/simulation/modelsim/main_tb.vht}

vsim -t 1ps -L altera -L lpm -L sgate -L altera_mf -L altera_lnsim -L cycloneii -L rtl_work -L work -voptargs="+acc"  main_tb

add wave *
view structure
view signals
run 80 ns
