-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "11/16/2018 10:46:55"
                                                            
-- Vhdl Test Bench template for design  :  main
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY main_tb_vhd_tst IS
END main_tb_vhd_tst;
ARCHITECTURE main_arch OF main_tb_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL cifra0 : STD_LOGIC_VECTOR(6 DOWNTO 0) := "1111111";
SIGNAL cifra1 : STD_LOGIC_VECTOR(6 DOWNTO 0) := "1111111";
SIGNAL clk : STD_LOGIC := '0';
SIGNAL reset : STD_LOGIC := '0';
SIGNAL serial : STD_LOGIC := '1';
SIGNAL period :time := 20 ns;
COMPONENT main
	PORT (
	cifra0 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
	cifra1 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
	clk : IN STD_LOGIC;
	reset : IN STD_LOGIC;
	serial : IN STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : main
	PORT MAP (
-- list connections between master ports and signals
	cifra0 => cifra0,
	cifra1 => cifra1,
	clk => clk,
	reset => reset,
	serial => serial
	);
clock : PROCESS                                                                                   
BEGIN                                                        
		clk <= '0';
		WAIT FOR period/2;
		clk <= '1';
		WAIT FOR period/2;
			  
END PROCESS clock;                                           
always : PROCESS                                              
                                      
BEGIN
                                                         
	serial <= '1', '0' AFTER 2*period, '1' AFTER 3*period, '0' AFTER 4*period, '1' AFTER 6*period;
	reset <= '0';
   WAIT FOR 15*period;
		
END PROCESS always;                                          
END main_arch;
